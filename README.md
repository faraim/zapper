# README #

Steps necessary in setting up the application.

### What is this repository for? ###

* Zapper interview task
* Version 0.1
* ABC

### How do I get set up? ###

* 1) Download RabbitMQ client for your environment at https://www.rabbitmq.com/download.html
* 2) Download the erlang runtime environment for your environment at https://www.erlang.org/downloads
* 3) Download Microsoft SQL server for your environment at https://www.microsoft.com/en-us/sql-server/sql-server-downloads
* 3.1) Download Microsoft Visual Studio IDE for an easy application setup at https://visualstudio.microsoft.com/downloads/
* 4) Install the erlang runtime environment downloaded earlier
* 5) Install the rabbitMQ software on your environment. After a successful installation, rabbitMQ service should be available at http://localhost:15672/ on your environment
* 6) If you face challenges in spinning up the rabbitMQ services, consult their troubleshooting documentation at https://www.rabbitmq.com/troubleshooting.html
* 7) Install the Microsoft Visual Studio IDE and the Microsft SQL Database Environment
* 8) Import the scripts.sql database located in the root of your repository on to your Microsoft SQL Server instance
* 9) Clone the repository to your local
* 10) Open the ZapperClient folder on your local and double click the solution (.sln)file which should open up as an instance on your Microsoft Visual Studio IDE, Repeat the same procedure for the remaiining two components.
* 11) Configure your database environment varibles in ZapperConsumer settings file as well as your API endpoint, if not using the default in ZapperClient component.
* 12) After configuring your environment variables, On every component instance, click on build and then run, to spin up the services per each component.
### Troubleshooting ###

* Make sure all your Database and API service environments reflect your current running environment

### Who do I talk to? ###

* Repo owner or admin at faraimrape@gmail.com
