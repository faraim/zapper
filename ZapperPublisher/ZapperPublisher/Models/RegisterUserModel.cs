﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace ZapperPublisher.Models
{
    //Structure Registration Class for Publisher Model
    public class RegisterUserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Bio { get; set; }
    }
}
