﻿using System.Text;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;
using ZapperPublisher.Models;

namespace ZapperPublisher.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //API Controller for external requests
    public class RegisterUserController : ControllerBase
    {
        [HttpPost]
        public void Post([FromBody]RegisterUserModel Registration)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "RegistrationQueue",
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                string message = "{" + "Name:" + Registration.FirstName + "," + "Surname:" + Registration.LastName + "," + "Phone:" + Registration.PhoneNumber + "," + "Email:" + Registration.EmailAddress + "," + "Biography:" + Registration.Bio + "}";
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                                     routingKey: "RegistrationQueue",
                                     basicProperties: null,
                                     body: body);
            }
        }
    }
}