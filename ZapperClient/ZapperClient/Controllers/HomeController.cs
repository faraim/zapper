﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ZapperClient.Models;

namespace ZapperClient.Controllers
{
    public class HomeController : Controller
    {
        //Default controller method
        public IActionResult Index()
        {
            return View();
        }
    
        //HTTP Post Controller method
        [HttpPost]
        public async Task<IActionResult> Index(RegisterUserModel registerUserModel)
        {
            RegisterUserModel receivedUser = new RegisterUserModel();
            using (var httpClient = new HttpClient())
            {
          
                StringContent content = new StringContent(JsonConvert.SerializeObject(registerUserModel), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync(Properties.Resources.API_URL, content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    try
                    {
                        receivedUser = JsonConvert.DeserializeObject<RegisterUserModel>(apiResponse);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Result = "Registration failed, Check availability of your services";
                        return View();
                    }
                }
            }
            ViewBag.Result = "Registration message sent";
            return View(receivedUser);
        }
    }
}
