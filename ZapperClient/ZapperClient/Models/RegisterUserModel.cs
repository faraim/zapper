﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace ZapperClient.Models
{
    //Structure Registration model class
    public class RegisterUserModel
    {
        [Required(ErrorMessage = "Please enter First Name")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter Last Name")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Phone Number")]
        [DisplayName("Phone Number")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please enter Email Address")]
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please enter Biography")]
        [DisplayName("Biography")]
        public string Bio { get; set; }
    }
}
