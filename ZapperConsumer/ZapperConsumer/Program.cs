﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;


class Program
{
    static void Main(string[] args)
    {
        //Get the the rabbitMQ connection
        var factory = new ConnectionFactory() { HostName = "localhost" };
        string queueName = "RegistrationQueue";
        var rabbitMqConnection = factory.CreateConnection();
        var rabbitMqChannel = rabbitMqConnection.CreateModel();

        rabbitMqChannel.QueueDeclare(queue: queueName,
                             durable: true,
                             exclusive: false,
                             autoDelete: false,
                             arguments: null);

        rabbitMqChannel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

        int messageCount = Convert.ToInt16(rabbitMqChannel.MessageCount(queueName));
        Console.WriteLine(" Listening to the queue for new messages. User registration channel had {0} messages on the queue", messageCount);

        //Loop through model body
        var consumer = new EventingBasicConsumer(rabbitMqChannel);
        consumer.Received += (model, ea) =>
        {
            var body = ea.Body;
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine(" Registration received: " + message);
            rabbitMqChannel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            Thread.Sleep(1000);

            //Strip down variables off the consumed message queue
            var StripString = message.Trim('{', '}').Split(',').Select(s => s.Trim().Split(':')).ToDictionary(a => a[0], a => a[1]);
           
            var Name = StripString["Name"];
            var Surname = StripString["Surname"];
            var Phone = StripString["Phone"];
            var Email = StripString["Email"];
            var Biography = StripString["Biography"];

            // Populate database with message queue entries
            using (var connection = new SqlConnection(ZapperConsumer.Properties.Settings.Default.ZapperConnect))
            {
                connection.Open();
                var sql = "INSERT INTO registration(FirstName, LastName, PhoneNumber, EmailAddress, Bio) VALUES(@FirstName, @LastName, @PhoneNumber, @EmailAddress, @Bio)";
                using (var cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.AddWithValue("@FirstName", Name);
                    cmd.Parameters.AddWithValue("@LastName", Surname);
                    cmd.Parameters.AddWithValue("@PhoneNumber", Phone);
                    cmd.Parameters.AddWithValue("@EmailAddress", Email);
                    cmd.Parameters.AddWithValue("@Bio", Biography);
                    cmd.ExecuteNonQuery();
                }
            }

        };
        rabbitMqChannel.BasicConsume(queue: queueName,
                             autoAck: false,
                             consumer: consumer);

        Thread.Sleep(1000 * messageCount);
        Console.ReadLine();
    }
}
